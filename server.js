const express = require('express')
const bodyParser = require('body-parser')
const Sequelize = require('sequelize')
const sequelize = new Sequelize('citations', 'root', '1234', {
  host: 'localhost',
  use_env_variable: "DATABASE_URL",
  dialect: "mysql"
});

let Citation = sequelize.define('citationsTable', {
  citation: {
    allowNull: false,
    type: Sequelize.STRING
  },
  date: {
    allowNull: false,
    type: Sequelize.DATE
  }
})

let Type = sequelize.define('typeTable', {
  citation_type: {
    allowNull: false,
    type: Sequelize.STRING
  }
})

let User = sequelize.define('userTable', {
  firstname: {
    allowNull: false,
    type: Sequelize.STRING
  },
  lastname: {
    allowNull: false,
    type: Sequelize.STRING,
  },
  username: {
    allowNull: true,
    type: Sequelize.STRING,
  },
  password: {
    allowNull: true,
    type: Sequelize.STRING,
  },
  email: {
    allowNull: false,
    type: Sequelize.STRING
  },
  sex: {
    allowNull: true,
    type: Sequelize.STRING,
  }
})

let app = express()

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});

app.use(bodyParser.json())

User.hasMany(Citation, {
  foreignKey: 'user_id'
})

Citation.belongsTo(User, {
  foreignKey: 'user_id',
})

Type.hasMany(Citation, {
  foreignKey: 'type_id'
})

Citation.belongsTo(Type, {
  foreignKey: 'type_id'
})

app.get('/create', (req, res) => {
  sequelize
    .sync({
      force: true
    })
    .then(() => {
      res.status(201).send('Tables created')
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('Some error...')
    })
})

app.get('/citations/:id', (req, res) => {
  Citation
    .findAll({
      attributes: ['id', 'citation', 'date', 'user_id', 'type_id'],
      where: {
        user_id: req.params.id
      }
    })
    .then((citations) => {
      res.status(200).send(citations)
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('Some error...')
    })
})

app.get('/users/:username', (req, res) => {
  User
    .find({
      attributes: ['id', 'firstname','lastname','username','password','email','sex'],
      where: {
        username: req.params.username
      }
    })
    .then((users) => {
      res.status(200).send(users)
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('Some error...')
    })
})

app.get('/types', (req, res) => {
  Type
    .findAll({
      attributes: ['id', 'citation_type']
    })
    .then((types) => {
      res.status(200).send(types)
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('Some error...')
    })
})

app.post('/user', (req, res) => {
  User
    .create(req.body)
    .then(() => {
      res.status(201).send('User added')
    })
    .catch((error) => {
      console.warn(error)
      console.log(req.body)
      res.status(500).send('Some error...')

    })
})

app.post('/citation', (req, res) => {
  Citation
    .create(req.body)
    .then(() => {
      res.status(201).send('Citation added')
    })
    .catch((error) => {
      console.warn(error)
      console.log(req.body)
      res.status(500).send('Some error...')

    })
})

app.delete('/citation/:id', (req, res) => {
  Citation
    .find({
      where: {
        id: req.params.id
      }
    })
    .then((citation) => {
      return citation.destroy()
    })
    .then(() => {
      res.status(201).send('Citation deleted.')
    })
    .catch((error) => {
      console.warn(error)
      res.status(500).send('Some error...')
    })
})

app.listen(8000)
