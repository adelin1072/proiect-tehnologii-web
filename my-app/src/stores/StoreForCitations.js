import axios from 'axios'

const SERVER_NAME = 'http://localhost:8000'

class StoreForCitations {
  constructor(ee) {
    this.content = []
    this.ee = ee
  }

  getCitations(completeUser) {
    console.log(completeUser);
    axios(SERVER_NAME + '/citations/' + completeUser.id)
      .then((response) => {
        this.content = response.data
        console.log(response.data);
        this.ee.emit('CITATIONS_GET')
      })
      .catch((error) => console.warn(error))
  }

  createCitation(citation,completeUser) {
  axios.post(SERVER_NAME + '/citation', citation)
      .then(() => this.getCitations(completeUser))
      .catch((error) => console.warn(error))
  }

  deleteCitation(citationId,completeUser) {
  axios.delete(SERVER_NAME + '/citation/' + citationId)
      .then(() => this.getCitations(completeUser))
      .catch((error) => console.warn(error))
  }
}

export default StoreForCitations
