import axios from 'axios'

const SERVER_NAME = 'http://localhost:8000'

class StoreForUsers {
  constructor(ee) {
    this.content = []
    this.ee = ee
  }

  getUser(username) {
    axios(SERVER_NAME + '/users/' + username)
      .then((response) => {
        this.content = response.data
        console.log(response.data);
        this.ee.emit('USER_GET')
      })
      .catch((error) => console.warn(error))
  }

  createOne(completeUser) {
  axios.post(SERVER_NAME + '/user', completeUser)
      .then()
      .catch((error) => console.warn(error))
  }
}

export default StoreForUsers
