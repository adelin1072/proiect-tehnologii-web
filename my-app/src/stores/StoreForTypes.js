import axios from 'axios'

const SERVER_NAME = 'http://localhost:8000'

class StoreForTypes {
  constructor(ee) {
    this.content = []
    this.ee = ee
  }

  getTypes() {
    axios(SERVER_NAME + '/types')
      .then((response) => {
        this.content = response.data
        console.log(response.data);
        this.ee.emit('TYPES_GET')
      })
      .catch((error) => console.warn(error))
  }
}

export default StoreForTypes
