import React, { Component } from 'react';
import Register from './Register'
import Login from './Login'
import {EventEmitter} from 'fbemitter'
import StoreForUsers from '../stores/StoreForUsers'

const ee = new EventEmitter()
const storeForUsers = new StoreForUsers(ee)

class Second extends Component {
  constructor(props) {
    super(props);

    this.checkUser = this.checkUser.bind(this);
    this.addUser = this.addUser.bind(this);
  }

  addUser(completeUser) {
    storeForUsers.createOne(completeUser);
  }

  checkUser(username, password) {
    storeForUsers.getUser(username);
    ee.addListener('USER_GET', () => {
      var completeUser = storeForUsers.content;
      if(completeUser.password === password) {
        sessionStorage.setItem('completeUser', JSON.stringify(completeUser));
        this.props.setLoggedTrue(completeUser);
      }  
    })
  }

  render() {
    return (
      <div>
      <br />
      <br />
      {this.props.userWhatToRegister ?
        <Register getLastId={this.getLastId} setLoggedTrue={this.props.setLoggedTrue} addUser={this.addUser} setIsRegisteringFalse={this.props.setIsRegisteringFalse} /> :
        <Login checkUser={this.checkUser} setIsRegisteringTrue={this.props.setIsRegisteringTrue}/>
      }
      </div>
    );
  }
  
}

export default Second;
