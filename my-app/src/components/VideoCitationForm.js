import React, { Component } from 'react';

class VideoCitationForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
        film_title : '',
        director_name : '',
        writer_name : '',
        year : ''
    }

    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })

    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
  }

  handleClearForm(eve) {  
    eve.preventDefault();
    this.setState({
        film_title : '',
        director_name : '',
        writer_name : '',
        year : ''
    });
  }
  handleSubmit (eve) {
    let citation = {
      'citation':this.state.writer_name + ". " + this.state.film_title + " " + this.state.year + ". Directed by " + this.state.director_name + ".",
       'date': new Date(),
       'user_id': this.props.completeUser.id,
       'type_id': 2
    }
      this.props.addCitation(citation)
      this.handleClearForm(eve);
      console.log("Citation was created");
  }

  render() {
    return (
        <div>
          Add a new Video Citation
           <br />
           <br />
          <form>
            <input value={this.state.film_title} placeholder="film_title" className="form-control" type="text" name="film_title" onChange={this.handleChange} required/>
            <input value={this.state.director_name} placeholder="director_name" className="form-control" type="text" name="director_name" onChange={this.handleChange} required/>
            <input value={this.state.writer_name} placeholder="writer_name" className="form-control" type="text" name="writer_name" onChange={this.handleChange} required/>
            <input value={this.state.year} placeholder="year" className="form-control" type="number" name="year" onChange={this.handleChange} required/>
            <br />
            <button type="button" className="btn btn-xs btn-primary" onClick={(eve) => {this.handleSubmit(eve)}}>Add video citation</button>
          </form>
        </div>
    ) 
  }
}

export default VideoCitationForm;
