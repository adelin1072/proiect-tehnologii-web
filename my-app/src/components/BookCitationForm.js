import React, { Component } from 'react';

class BookCitationForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
        author_firstname : '',
        author_lastname : '',
        title : '',
        publisher : '',
        year : ''
    }

    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })

    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
  }

  handleClearForm(eve) {  
    eve.preventDefault();
    this.setState({
      author_firstname : '',
        author_lastname : '',
        title : '',
        publisher : '',
        year : ''
    });
  }

  handleSubmit (eve) {
    let citation = {
      'citation':this.state.author_lastname + " " + this.state.author_firstname + ". " 
       + this.state.title + ". " + this.state.publisher + ", " + this.state.year,
       'date': new Date(),
       'user_id': this.props.completeUser.id,
       'type_id': 1
    }
      this.props.addCitation(citation)
      this.handleClearForm(eve);
      console.log("Citation was created");
  }

  render() {
    return (
        <div>
          Add a new Book Citation
           <br />
           <br />
          <form>
            <input value={this.state.author_firstname} placeholder="author_firstname" 
            className="form-control" type="text" name="author_firstname" onChange={this.handleChange} required/>
            <input value={this.state.author_lastname} placeholder="author_lastname" 
            className="form-control" type="text" name="author_lastname" onChange={this.handleChange} required/>
            <input value={this.state.title} placeholder="title" 
            className="form-control" type="text" name="title" onChange={this.handleChange} required/>
            <input value={this.state.publisher} placeholder="publisher" 
            className="form-control" type="text" name="publisher" onChange={this.handleChange} required/>
            <input value={this.state.year} placeholder="year" 
            className="form-control" type="number" name="year" onChange={this.handleChange} required/>
            <br />
            <button type="button" className="btn btn-xs btn-primary" onClick={(eve) => {this.handleSubmit(eve)}}>Add book citation</button>
          </form>
        </div>
    ) 
  }
}

export default BookCitationForm;
