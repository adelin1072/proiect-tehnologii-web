import React, { Component } from 'react';

class JournalCitationForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
        article_title : '',
        journal_title : '',
        author : '',
        year : ''
    }

    this.handleChange = (event) => {
      this.setState({
        [event.target.name] : event.target.value
      })

    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleClearForm = this.handleClearForm.bind(this);
  }

  handleClearForm(eve) {  
    eve.preventDefault();
    this.setState({
        article_title : '',
        journal_title : '',
        author : '',
        year : ''
    });
  }

  handleSubmit (eve) {
    let citation = {
      'citation':this.state.author + ". " + this.state.article_title + " " + this.state.journal_title + ", " + this.state.year,
       'date': new Date(),
       'user_id': this.props.completeUser.id,
       'type_id': 3
    }
      this.props.addCitation(citation)
      this.handleClearForm(eve);
      console.log("Citation was created");
  }

  render() {
    return (
        <div>
          Add a new Journal Citation
           <br />
           <br />
          <form>
            <input value={this.state.article_title} placeholder="article_title" 
            className="form-control" type="text" name="article_title" onChange={this.handleChange} required/>
            <input value={this.state.journal_title} placeholder="journal_title" 
            className="form-control" type="text" name="journal_title" onChange={this.handleChange} required/>
            <input value={this.state.year} placeholder="year" 
            className="form-control" type="number" name="year" onChange={this.handleChange} required/>
            <input value={this.state.author} placeholder="author" 
            className="form-control" type="text" name="author" onChange={this.handleChange} required/>
            <br />
            <button type="button" className="btn btn-xs btn-primary" onClick={(eve) => {this.handleSubmit(eve)}}>Add journal citation</button>
          </form>
        </div>
    ) 
  }
}

export default JournalCitationForm;
