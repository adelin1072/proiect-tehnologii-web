import React, { Component } from 'react';

class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
        firstname : '',
        lastname : '',
        username : '',
        password : '',
        email: '',
        sex: 'M'
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange (event) {
      this.setState({
        [event.target.name] : event.target.value
      })
  }

  handleSubmit () {
      var completeUser = {
          firstname : this.state.firstname,
          lastname : this.state.lastname,
          username : this.state.username,
          password : this.state.password,
          email : this.state.email,
          sex : this.state.sex
      }

      this.props.addUser(completeUser)
      this.props.setIsRegisteringFalse();
      sessionStorage.setItem('completeUser', JSON.stringify(completeUser));
      console.log("Account was created");
  }

  render() {
    return (
        <div>
        Here you can make a new account
        <br />
        <br />
        <form ref={(ev) => this.form = ev}>
          <label>Firstname</label>
          <input value={this.state.firstname} className="form-control" type="text" name="firstname" onChange={this.handleChange} />
          <br />
          <label>Lastname</label>
          <input value={this.state.lastname} className="form-control" type="text" name="lastname" onChange={this.handleChange} />
          <br />
          <label>E-mail</label>
          <input value={this.state.email} className="form-control" type="email" name="email" onChange={this.handleChange} />
          <br />
          <label>Sex</label>
          <select name="sex" onChange={this.handleChange} value={this.state.sex} className="form-control">
            <option>M</option><option>F</option>
          </select>
          <br />
          <label>Username</label>
          <input value={this.state.username} className="form-control" type="text" name="username" onChange={this.handleChange} />
          <br />
          <label>Password</label>
          <input value={this.state.password} className="form-control" type="password" name="password" onChange={this.handleChange} />

          <br />
          <button type="button" className="btn btn-xs btn-success" onClick={this.handleSubmit}>Add new account</button>
          <button type="button" className="btn btn-xs btn-warning " onClick={this.props.setIsRegisteringFalse}>Back</button>
            </form>
        </div>
      ) 
  }
}

export default Register;
