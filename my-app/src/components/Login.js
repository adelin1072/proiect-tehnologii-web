import React, { Component } from 'react';

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
        username : "",
        password : ""
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleChange(event) {
      this.setState({
        [event.target.name] : event.target.value
      })
  }

  handleSubmit () {
    this.props.checkUser(this.state.username, this.state.password);
  }

  render() {
    return (
        <div className="col-sm-4">
          Here you can login
           <br />
           <br />
          <form ref={(ev) => this.form = ev}>
            <input value={this.state.username} placeholder="Username" 
            className="form-control" type="text" name="username" onChange={this.handleChange} required/>
            <input value={this.state.password} placeholder="Password" 
            className="form-control" 
              type="password" name="password" onChange={this.handleChange} required/>
              <br />
            <button type="button" className="btn btn-xs btn-success" onClick={this.handleSubmit}>Login</button>
            <button type="button" className="btn btn-xs btn-warning" onClick={this.props.setIsRegisteringTrue}>Register</button>
          </form>
        </div>
    ) 
  }
}

export default Login;
