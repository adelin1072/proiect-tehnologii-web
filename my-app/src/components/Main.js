import React, { Component } from 'react';
import BookCitationForm from './BookCitationForm'
import JournalCitationForm from './JournalCitationForm'
import VideoCitationForm from './VideoCitationForm'
import CitationsList from './CitationsList'
import {EventEmitter} from 'fbemitter'
import StoreForCitations from '../stores/StoreForCitations'
import StoreForTypes from '../stores/StoreForTypes'

const ee = new EventEmitter()
const storeForCitations = new StoreForCitations(ee)
const storeForTypes = new StoreForTypes(ee)

class Main extends Component {
  constructor(props) {
    super(props);

    this.state = {
      citations: [],
      types: []
    }

    this.deleteCitation = this.deleteCitation.bind(this);
    this.addCitation = this.addCitation.bind(this);
    this.logout = this.logout.bind(this);
  }

  componentDidMount() {
    storeForCitations.getCitations(this.props.completeUser);
    ee.addListener('CITATIONS_GET', () => {
      this.setState({
        citations : storeForCitations.content
      })
    })

    storeForTypes.getTypes();
    ee.addListener('TYPES_GET', () => {
      this.setState({
        types : storeForTypes.content
      })
    })
  }

  deleteCitation(citation) {
    storeForCitations.deleteCitation(citation.id,this.props.completeUser);
  }

  addCitation(citation) {
    storeForCitations.createCitation(citation,this.props.completeUser);
  }

  logout() {
    this.props.setLoggedFalse();
  }

  render() {
    return (
      <div>
      <br />
      <br />
        <div className="col-sm-6">
          <button className="btn btn-xs btn-success pull-right" onClick={this.logout}>Logout</button>
          <br />
          <CitationsList types={this.state.types} citations={this.state.citations} deleteCitation={this.deleteCitation}/>
        </div>
        <div className="col-sm-6">
          <BookCitationForm addCitation={this.addCitation} completeUser={this.props.completeUser}/>
          <br />
          <JournalCitationForm addCitation={this.addCitation} completeUser={this.props.completeUser}/>
          <br />
          <VideoCitationForm addCitation={this.addCitation} completeUser={this.props.completeUser}/>
        </div>
      </div>
    );
  }
}

export default Main;
