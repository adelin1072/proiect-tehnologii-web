import React, { Component } from 'react'

class CitationsList extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }

    this.getTypeName = this.getTypeName.bind(this);
  }

  getTypeName(typeId) {
    if(this.props.types !== null && this.props.types.length > 0) {
      return this.props.types.filter((type) => type.id === typeId)[0].citation_type;
    } else {
      return '';
    }
  }

  render() {
      return (
        <div>
        Citations:
          <table className="table">
            <thead>
                <tr>
                  <th>Index</th>
                  <th>Citation</th>
                  <th>Type</th>
                  <th>Delete</th>
                </tr>
              </thead>
              <tbody>
              {this.props.citations.map((citation,index)=> (
                <tr key={index}>
                  <td>{index}</td>
                  <td>{citation.citation}</td>
                  <td>{this.getTypeName(citation.type_id)}</td>
                  <td>
                    <button type="button" className="btn btn-xs btn-warning" onClick={() => this.props.deleteCitation(citation)}>Delete</button>
                  </td>
                </tr>
              ))}
              </tbody>
          </table>
        </div>
      ) 
  }
}

export default CitationsList
