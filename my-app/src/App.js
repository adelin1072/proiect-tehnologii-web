import React, { Component } from 'react';
import './App.css';
import Main from './components/Main'
import Second from './components/Second'

class App extends Component {

  constructor(props) {
    super(props);
    var completeUser = JSON.parse(sessionStorage.getItem('completeUser'));
    this.state = {
      userLoggedIn: completeUser,
      userWhatToRegister: false
    }

    this.setIsRegisteringTrue = this.setIsRegisteringTrue.bind(this);
    this.setIsRegisteringFalse = this.setIsRegisteringFalse.bind(this);
    this.setLoggedTrue = this.setLoggedTrue.bind(this);
    this.setLoggedFalse = this.setLoggedFalse.bind(this);
  }

  setLoggedFalse () {
    sessionStorage.clear();
    this.setState({userLoggedIn: null});
  }

  setLoggedTrue(completeUser) {
        this.setState({userLoggedIn: completeUser});
  }

  setIsRegisteringTrue () {
    this.setState(
        {
          userWhatToRegister: true
        }
      );
  }

  setIsRegisteringFalse() {
    this.setState(
        {
          userWhatToRegister: false
        }
      );
  }

  render() {
    return (
      <div className="container">
      {this.state.userLoggedIn !== null ?
        <Main completeUser={this.state.userLoggedIn} setLoggedFalse={this.setLoggedFalse}/> : <Second setLoggedTrue={this.setLoggedTrue} setIsRegisteringFalse={this.setIsRegisteringFalse} setIsRegisteringTrue={this.setIsRegisteringTrue} userWhatToRegister={this.state.userWhatToRegister}/>
      }
      </div>
    );
  }
}

export default App;
