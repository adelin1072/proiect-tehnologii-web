Despre aplicatie:

	Aplicatia ce va fi realizata permite utilizatorilor sa isi realizeze conturi noi si sa se logheze in aceasta. 
	Pentru realizarea unui cont noi utilizatorul va trebui sa introduce detalii precum nume, prenume, username, parola, e-mail si numar de telefon. Campurile acestea vor avea validari, pentru a nu permite realizarea de conturi cu e-mail-uri sau numere de telefon invalide.
Dupa ce acestia se autentifica, pot cita o carte, un video/film sau un jurnal. Pentru acest pas, utilizatorul va fi nevoit sa introduca o serie de detalii despre obiectul pe care isi propune sa il citeze. Unele dintre acesta sunt obligatorii, iar altele optionale.
Cateva exemple de detalii, in cazul actiunii de citare a unei carti, sunt: titlul cartii, autorul acesteia, anul publicarii si editura. Detaliile sunt diferinte in functie de citatia dorita, dar pot exista si campuri comune, cum ar fi titlul sau anul.
Daca toate campurile obligatorii sunt completate si butonul de trimitere este apasat, in cateva secunde citatia noua va fi adaugata la tabel, iar campurile completate se vor reseta.
	Pentru a pastra un registru al citatiilor, in caz ca utilizatorul doreste sa le refoloseasca, aplicatia va detine si aceasta functionalitate, sub forma de tabel.
	Utilizatorul poate oricand sa stearga citatiile din istoricul acestuia, pentru a pastra doar citatiile pe care acesta vrea sa le reutilizeze sau in cazul in care acesta realizeaza o citatile eronata.

Actiunile utilizatorului:

❖	Realizarea unui cont.
❖	Autentificarea cu un cont realizat in aplicatie.
❖	Citarea unei carti, a unui videoclip/film sau a unui jurnal.
❖	Completarea datelor necesare pentru citarea dorita (unele obligatorii, altele optionale).
❖	Vizualizarea unui istoric al citatiilor.
❖	Stergerea citatiilor din istoric.

